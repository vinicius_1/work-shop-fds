
from django.views.generic import ListView, CreateView, DeleteView
from .forms import PessoaForm
from .models import  Pessoa
from django.urls import reverse_lazy
# LOCAL ONDE É A PONTE ENTRE OS MODELOS (BANCO DE DADOS) SE COMUNICARÁ COM OS TEMPLATES (PÁGINA WEB),
# PODENDO HAVER LÓGICA COMO VALIDAÇÃO DOS DADOS. PARA INICIAR O SERVER, UTILIZA:
# python manage.py runserver



class PessoaListView(ListView):
    model = Pessoa #criamos a view generica
    template_name = "pessoas/Listar_pessoas.html"
    context_object_name = "pessoas"



class PessoaCreateView(CreateView):
    model = Pessoa
    template_name = "pessoas/criar_pessoa.html"
    form_class = PessoaForm
    success_url = reverse_lazy("pessoas_pessoa_list")




class PessoaDeleteView(DeleteView):
    model = Pessoa
    success_url = reverse_lazy("pessoas_pessoa_list")

from django.urls import path
from .views import PessoaListView,PessoaCreateView, PessoaDeleteView

urlpatterns = [
    path ('',PessoaListView.as_view(), name = "pessoas_pessoa_list"),#convertendo para uma função que recebe uma requisição
    path ('criar/',PessoaCreateView.as_view(), name = 'pessoas_pessoa_create'),
    path ("<int:pk>/delete", PessoaDeleteView.as_view(), name="pessoas_pessoa_delete")#PK = Primary key -> ID- indentificador do modelo(pessoa)
]